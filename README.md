# Juno

Juno is a space explorer.  Juno is bootstraped with Atlas Starter Kit, built on top of create-react-app.

## Getting started

Please make sure you have [node](https://nodejs.org/en/download/) installed in your system.

Please run the following commands to clone the project, install dependencies and start the application.

```bash
git clone https://evanfreymiller@bitbucket.org/evanfreymiller/juno.git   # clone the project
cd juno # cd into the project directory
npm install  # install dependencies
npm run server # run mock server
npm run start  # start the project
```

## Run Tests

To run tests, run the following command

```npm run test```

##Production Build

To make a production build, run the following command

```npm run build```

You can run the local express server to view the production build by running the following command

```npm run server```

Open your browser to the following address

```localhost:2300```

##Known Bugs

Known bugs are as follows

```The url does not update on load of the / endpoint```


