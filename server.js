var mockService = require('osprey-mock-service')
var express = require('express')
var parser = require('raml-1-parser')
var path = require('path')
var osprey = require('osprey')
var PORT = 2300;
var app = express()

app.use(express.static(__dirname + '/build'));
app.get('/', function (request, response) {
  response.sendFile(path.resolve(__dirname, 'index.html'));
});

parser.loadRAML(path.join(__dirname, 'api.raml'), { rejectOnErrors: true })
  .then(function (ramlApi) {
    var raml = ramlApi.expand(true).toJSON({ serializeMetadata: false })
    app.use(osprey.server(raml))
    app.use(mockService(raml))
    app.listen(PORT);
    console.log(`Listening on ${PORT}`)
  })