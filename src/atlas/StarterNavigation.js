import ArrowleftIcon from '@atlaskit/icon/glyph/arrow-left';
import AtlassianIcon from '@atlaskit/icon/glyph/atlassian';
import GearIcon from '@atlaskit/icon/glyph/settings';
import Nav, { AkContainerTitle, AkNavigationItem } from '@atlaskit/navigation';
import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router';
import AccountDropdownMenu from '../atlas/AccountDropdownMenu';
import HelpDropdownMenu from '../atlas/HelpDropdownMenu';
import { getAllSpaces } from './../api/SpacesAPI';
import { JunoContext } from './../modules/Context';
import classnames from 'classnames';


export default class StarterNavigation extends React.Component {
  componentWillMount() {
    let spaces = getAllSpaces();
    spaces.then((response) => {
      let { items } = response.data;
      let navLinks = [];
      items.forEach(element => {
        let { fields, sys } = element;
        let { title } = fields;
        let { id } = sys;
        navLinks.push({ title: title, url: '/space', id: id, icon: GearIcon, fields: fields })
      });

      this.setState({
        navLinks: navLinks
      }, () => {
        this.context.setSpace(navLinks[0].id, navLinks[0].fields)
      })
    }).catch((error) => {
      this.context.setAppError(error)
    })
  }

  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object,
  };
  static contextType = JunoContext;

  constructor(props) {
    super(props)
    this.state = {
      navLinks: []
    }
  }

  shouldComponentUpdate(nextProps, nextContext) {
    return true;
  };

  render() {
    const backIcon = <ArrowleftIcon label="Back icon" size="medium" />;
    const globalPrimaryIcon = <AtlassianIcon label="Atlassian icon" size="xlarge" />;

    return (
      <Nav
        onResize={this.props.onNavResize}
        containerHeaderComponent={() => (
          <AkContainerTitle
            href="/"
            icon={
              <img alt="juno logo" src="https://upload.wikimedia.org/wikipedia/commons/3/32/Juno_mission_insignia.svg" />
            }
            text="Juno"
          />
        )}
        globalPrimaryIcon={globalPrimaryIcon}
        globalPrimaryItemHref="/"
        globalAccountItem={AccountDropdownMenu}
        globalHelpItem={HelpDropdownMenu}
        onSearchDrawerOpen={() => this.openDrawer('search')}
        onCreateDrawerOpen={() => this.openDrawer('create')}
      >
        {
          this.state.navLinks.map(link => {
            let { icon, id, title, url, fields } = link;
            return (
              <Link key={id} to={`${url}/${id}`}>
                <AkNavigationItem
                  icon={icon}
                  text={title}
                  isSelected={id === this.context.currentSpaceID}
                  onClick={() => this.context.setSpace(id, fields)}
                />
              </Link>
            );
          }, this)
        }
      </Nav>
    );
  }
}
