import React from 'react';
import './Atlas.css'
export default () => (
  <section className='juno-home' style={{marginBottom: '10px'}}>
    <div>Your space explorer</div>
    <img alt="juno logo" src="https://upload.wikimedia.org/wikipedia/commons/3/32/Juno_mission_insignia.svg" />
  </section>
);
