import styled from 'styled-components';
import { gridSize } from '@atlaskit/theme';

const PageTitle = styled.h1`
  margin-bottom: ${gridSize() * 2}px;
  text-align: center;
  font-size: 72px;
  font-weight: 900;
`;

export default PageTitle;
