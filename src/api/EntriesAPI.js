import axios from 'axios';

const getEntriesForSpaceID = (spaceID) => {
    return axios.get(`/space/ ${spaceID} /entries`)
        .then(function (response) {
            return response
        })
        .catch(function (error) {
            return error
        });
}
export { getEntriesForSpaceID }