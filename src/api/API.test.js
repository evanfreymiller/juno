import React from 'react';
import ReactDOM from 'react-dom';

const USER = {
    "fields": {
        "name": "Alana Atlassian",
        "role": "Author"
    },
    "sys": {
        "id": "4FLrUHftHW3v2BLi9fzfjU",
        "type": "User"
    }
}

import { getUserName } from './UserAPI';

describe('check that users function returns expected object', () => {
    it('it should have an object matching the USER const', () => {
        return getUserName('4FLrUHftHW3v2BLi9fzfjU').then(data => expect(data).toEqual(USER))
      });
})

