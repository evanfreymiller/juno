import axios from 'axios';

const getAssetsForSpaceID = (spaceID) => {
    return axios.get(`/space/ ${spaceID} /assets`)
        .then(function (response) {
            return response
        })
        .catch(function (error) {
            return error
        });
}
export { getAssetsForSpaceID }