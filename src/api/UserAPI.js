import axios from 'axios';

const getUserName = (userID) => {
    return axios.get(`/users/ ${userID}`)
        .then(function (response) {
            let { fields } =  response.data;
            let { name } = fields;
            return name
        })
        .catch(function (error) {
            return error
        });
}
export { getUserName }