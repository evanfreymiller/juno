const convertISOTimestamp = (timestamp) => {
    let date = new Date(timestamp).getDate();
    let month = new Date(timestamp).getMonth();
    let year = new Date(timestamp).getFullYear();
    let time = `${month+1}/${date}/${year}`;
    return time;
}
function sortABC(items, header){
    items.sort(function (a, b) {
        if (a.content[(header.toLowerCase()).replace(/\s/g, '')] < b.content[(header.toLowerCase()).replace(/\s/g, '')]) { return -1; }
        if (a.content[(header.toLowerCase()).replace(/\s/g, '')] > b.content[(header.toLowerCase()).replace(/\s/g, '')]) { return 1; }
        return 0;
      })
    return items;
}
function sortZYX(items, header){
    items.sort(function (a, b) {
        if (a.content[(header.toLowerCase()).replace(/\s/g, '')] > b.content[(header.toLowerCase()).replace(/\s/g, '')]) { return -1; }
        if (a.content[(header.toLowerCase()).replace(/\s/g, '')] < b.content[(header.toLowerCase()).replace(/\s/g, '')]) { return 1; }
        return 0;
      })
    return items;
}

function buildTableObject(response) {

    let itemsArray = response;

    let items = [];
    itemsArray.forEach(item => {
      let { fields, sys } = item;
      let { id, createdBy, updatedBy, updatedAt } = sys;
      let { title, contentType, fileName } = fields;
      items.push(
        {
          id: id,
          content: {
            title: title,
            contenttype: contentType,
            filename: fileName,
            createdby: createdBy,
            updatedby: updatedBy,
            lastupdated: updatedAt
          }
        }
      )
    });
    return items;
}
export { convertISOTimestamp, sortABC, sortZYX, buildTableObject };