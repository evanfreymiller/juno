import { convertISOTimestamp, sortABC, sortZYX, buildTableObject } from './Helper';
import { table, expectedTable } from './../../constants';
const itemsABCToBeSorted = [
    {content: {title: 'c'}},
    {content: {title: 'b'}},
    {content: {title: 'a'}}
]
const itemsABC = [
    {content: {title: 'a'}},
    {content: {title: 'b'}},
    {content: {title: 'c'}}
]
const itemsZYXToBeSorted = [
    {content: {title: 'a'}},
    {content: {title: 'b'}},
    {content: {title: 'c'}}
]
const itemsZYX = [
    {content: {title: 'c'}},
    {content: {title: 'b'}},
    {content: {title: 'a'}}
]

describe('check that time object is correct', () => {
    it('it should be MM/DD/YYYY', () => {
        let time = convertISOTimestamp("2015-05-18T11:29:46.809Z")
        expect(time).toEqual('5/18/2015')
      });
})

describe('check that abc sorting works', () => {
    it('it should be in abc order', () => {
        let sort = sortABC(itemsABCToBeSorted, 'title')
        expect(sort).toEqual(itemsABC)
      });
})

describe('check that abc sorting works', () => {
    it('it should be in abc order', () => {
        let sort = sortZYX(itemsZYXToBeSorted, 'title')
        expect(sort).toEqual(itemsZYX)
      });
})


describe('check that table object builds correctly', () => {
    it('it should match the existing schema', () => {
        let built = buildTableObject(table)
        expect(built).toEqual(expectedTable)
      });
})

