import React, { Fragment, Component } from 'react';
import {
    Cell,
} from '@atlaskit/table-tree';

import { getUserName } from './../api/UserAPI';
import { convertISOTimestamp } from './../helper/Helper';

class Cells extends Component {
    componentWillMount(){
        let { content, id } = this.props.item;
        let { createdby, updatedby, lastupdated } = content;
        let createdByPromise = getUserName(createdby)
        createdByPromise.then((name) => {
            this.setState({
                createdBy: name,
                updatedBy: name
            })
        })
    }
    constructor(props){
        super(props)
        this.state = {

        }
    }
    render(){
        if (this.props.type === 'entries') {
            let { content, id } = this.props.item;
            let { title, summary, lastupdated } = content;
            
            return (
                <Fragment>
                    <Cell width={150} singleLine>
                        {title}
                    </Cell>
                    <Cell>
                        {summary}
                    </Cell>
                    <Cell>
                        {this.state.createdBy}
                    </Cell>
                    <Cell>
                        {this.state.updatedBy}
                    </Cell>
                    <Cell>
                        {convertISOTimestamp(lastupdated)}
                    </Cell>
                </Fragment>
            )
        }
        if (this.props.type === 'assets') {
            let { content, id } = this.props.item;
            let { title, contenttype, filename, lastupdated } = content;
            
            return (
                <Fragment>
                    <Cell width={150} singleLine>
                        {title}
                    </Cell>
                    <Cell>
                        {contenttype}
                    </Cell>
                    <Cell>
                        {filename}
                    </Cell>
                    <Cell>
                        {this.state.createdBy}
                    </Cell>
                    <Cell>
                        {this.state.updatedBy}
                    </Cell>
                    <Cell>
                        {convertISOTimestamp(lastupdated)}
                    </Cell>
                </Fragment>
            )
        }
    }

}
export { Cells };