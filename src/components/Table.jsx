import React from 'react';
import TableTree,{ Headers, Header, Row } from '@atlaskit/table-tree';
import { Cells } from './Cells';
import { sortABC, sortZYX } from './../helper/Helper';
const Table = (props) => {
    return (
        <TableTree>
            <Headers>
                {
                    props.headers.map((header) => {
                        return(
                            <Header style={{cursor: 'pointer'}} onClick={() => props.updateSort(props.items, header)} width={150}>{header}</Header>
                        )
                    })
                }
            </Headers>
            {
                props.items.map((item) => {
                    let { content, id } = item;
                    let { title, summary, createdBy, updatedBy, lastUpdated } = content;
                    return (
                        <Row itemId={id} hasChildren={false}>
                            <Cells type={props.type} item={item}  />
                        </Row>
                    )
                })
            }
        </TableTree>

    )
}
export default Table;