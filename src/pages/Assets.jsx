import React, { Component } from 'react'
import { JunoContext } from './../modules/Context';
import { getAssetsForSpaceID } from './../api/AssetsAPI';
import Table from '../components/Table';
import { sortZYX, sortABC, buildTableObject } from '../helper/Helper';

class Assets extends Component {

  componentWillMount() {
    let entries = getAssetsForSpaceID(this.context.currentSpaceID);
    entries.then((response) => {
      let items = buildTableObject(response.data.items)
      this.setState({
        tableData: items,
        entries: response.data.items
      })
    }).catch((error) => {
      this.context.setAppError(error)
    })
  }
  updateSort = (items, header) => {
    let sorted;
    if(this.state.abc){
      sorted = sortZYX(items, header);
      this.setState({
        tableData: sorted,
        abc: false
      })
    }
    else{
      sorted = sortABC(items, header);
      this.setState({
        tableData: sorted,
        abc: true
      })
    }
  }
  static contextType = JunoContext;
  constructor(props) {
    super(props);
    this.state = {
      abc: true
    }
  }

  render() {
    const titleComp = <span>Title</span>
    if (!this.context.currentSpace || !this.state.tableData) {
      return null
    }
    else {
      return (
        <div style={{ minWidth: '100%' }}>
          <Table
            type="assets"
            updateSort={this.updateSort}
            headers={['Title', 'ContentType', 'FileName', 'Created By', 'Updated By', 'Last Updated']}
            columnWidths={['20%', '20%', '20%', '20%', '20%']}
            items={this.state.tableData}
            columns={['Title', 'ContentType', 'FileName', 'CreatedBy', 'Updatedy', 'LastUpdated']}
          />
        </div>

      )
    }
  }
}
export default Assets;
