import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Button, { ButtonGroup } from '@atlaskit/button';
import MainSection from '../atlas/MainSection';
import ContentWrapper from '../atlas/ContentWrapper';
import PageTitle from '../atlas/PageTitle';

export default class About extends Component {
  static contextTypes = {
    showModal: PropTypes.func,
    addFlag: PropTypes.func,
    onConfirm: PropTypes.func,
    onCancel: PropTypes.func,
    onClose: PropTypes.func,
  };

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Juno</PageTitle>
        <MainSection />
      </ContentWrapper>
    );
  }
}
