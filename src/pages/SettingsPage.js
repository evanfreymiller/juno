import React, { Component } from 'react';
import MainSection from '../atlas/MainSection';
import ContentWrapper from '../atlas/ContentWrapper';
import PageTitle from '../atlas/PageTitle';

export default class HomePage extends Component {
  render() {
    return (
      <ContentWrapper>
        <PageTitle>Settings</PageTitle>
        <MainSection />
      </ContentWrapper>
    );
  }
}
