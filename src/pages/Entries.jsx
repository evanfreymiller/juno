import React, { Component } from 'react'
import { JunoContext } from './../modules/Context';
import { getEntriesForSpaceID } from './../api/EntriesAPI';
import { getUserName } from './../api/UserAPI';

import Table from '../components/Table';
import { sortZYX, sortABC } from '../helper/Helper';

class Entries extends Component {

  componentWillMount() {
    let entries = getEntriesForSpaceID(this.context.currentSpaceID);
    entries.then((response) => {
      let itemsArray = response.data.items;

      let items = [];
      itemsArray.forEach(item => {
        let { fields, sys } = item;
        let { id, createdBy, updatedBy, updatedAt } = sys;
        let { title, summary } = fields;
        items.push(
          {
            id: id,
            content: {
              title: title,
              summary: summary,
              createdby: createdBy,
              updatedby: updatedBy,
              lastupdated: updatedAt
            }
          }
        )
      });
      this.setState({
        tableData: items,
        entries: response.data.items
      })
    }).catch((error) => {
      this.context.setAppError(error)
    })
  }
  updateSort = (items, header) => {
    let sorted
    if(this.state.abc){
      sorted = sortZYX(items, header);
      this.setState({
        tableData: sorted,
        abc: false
      })
    }
    else{
      sorted = sortABC(items, header);
      this.setState({
        tableData: sorted,
        abc: true
      })
    }
  }

  static contextType = JunoContext;
  constructor(props) {
    super(props);
    this.state = {
      abc: true
    }
  }

  render() {
    const titleComp = <span>Title</span>
    if (!this.context.currentSpace || !this.state.tableData) {
      return null
    }
    else {
      return (
        <div style={{ minWidth: '100%' }}>
          <Table
            type="entries"
            updateSort={this.updateSort}
            headers={['Title', 'Summary', 'Created By', 'Updated By', 'Last Updated']}
            columnWidths={['20%', '20%', '20%', '20%', '20%']}
            items={this.state.tableData}
            columns={['Title', 'Summary', 'CreatedBy', 'Updatedy', 'LastUpdated']}
          />
        </div>

      )
    }
  }
}
export default Entries;
