import React, { Component } from 'react';
import { JunoContext } from './../modules/Context';
import { Redirect } from 'react-router-dom';
import ContentWrapper from '../atlas/ContentWrapper';
import { TabContent, TabItem } from '@atlaskit/tabs';
import Tabs from '@atlaskit/tabs';
import Assets from './Assets';
import Entries from './Entries';



const tabs = [
  { label: 'Entries', content: <Entries /> },
  { label: 'Assets', content: <Assets /> },
,
];
class Space extends Component {
  componentWillMount() {

  }
  static contextType = JunoContext;
  constructor(props) {
    super(props);
    this.state = {

    }
  }
  
  render() {
    if (!this.context.currentSpace) {
      return null
    }
    else if(this.context.currentSpaceID === 'yadj1kx9rmg01' || this.context.currentSpaceID === 'yadj1kx9rmg02')
    {
      return (
        <ContentWrapper>
        <h1>{'Not Available'}</h1>
        <p>{'Sorry!  That ID is not available.'}</p>
      </ContentWrapper>
      )
    }
    else {
      const { title, description } = this.context.currentSpace
      return (
        <ContentWrapper>
          <h1>{title}</h1>
          <p>{description}</p>
          <Tabs
            tabs={tabs}
            onSelect={(tab, index) => console.log('Selected Tab', index + 1)}
          />
        </ContentWrapper>
      );
    }

  }
}

export default Space;
