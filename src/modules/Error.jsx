import React from 'react';
import Banner from '@atlaskit/banner';
import WarningIcon from '@atlaskit/icon/glyph/warning';
const Icon = <WarningIcon label="Warning icon" secondaryColor="inherit" />;

const Error = (props) => {
    return (
        <Banner icon={Icon} isOpen={props.error} appearance="error">
            Opps!  We seem to have hit an error.  Please try again later.
        </Banner>
    )
}
export default Error;