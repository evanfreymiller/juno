import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Flag, { FlagGroup } from '@atlaskit/flag';
import Modal from '@atlaskit/modal-dialog';
import Page from '@atlaskit/page';
import '@atlaskit/css-reset';
import { JunoContext } from './Context';
import StarterNavigation from '../atlas/StarterNavigation';
import MainRouter from './MainRouter';
import Error from './Error';

export default class App extends Component {
  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object,
  };

  static propTypes = {
    navOpenState: PropTypes.object,
    onNavResize: PropTypes.func,
  };

  static childContextTypes = {
    showModal: PropTypes.func,
    addFlag: PropTypes.func,
  }
  setSpace = (id, space) => {
    this.setState({
      currentSpaceID: id,
      currentSpace: space
    })
  }
  setAppError = (error) => {
    console.log(error);
    this.setState({
      error: true,
      error: error
    })
  }
  constructor(props) {
    super(props)
    this.state = {
      error: false
    }
  }

  render() {
    return (
      <JunoContext.Provider
        value={{
          spaces: this.state.spaces,
          setSpace: this.setSpace,
          currentSpaceID: this.state.currentSpaceID,
          currentSpace: this.state.currentSpace,
          error: this.state.error,
          setAppError: this.setAppError
        }}>
        <Page navigation={<StarterNavigation />} >
          <Error error={this.state.error} />
          {this.props.children}
        </Page>
      </JunoContext.Provider>
    );
  }
}
