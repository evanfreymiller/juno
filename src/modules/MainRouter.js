import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Router, Route, browserHistory, Redirect } from 'react-router';
import App from './App';
import HomePage from '../pages/About';
import SettingsPage from '../pages/SettingsPage';
import Space from '../pages/Space';
import { JunoContext } from './Context';

export default class MainRouter extends Component {
  componentWillMount(){
    
  }
  static contextType = JunoContext;

  constructor() {
    super();
    this.state = {
      navOpenState: {
        isOpen: true,
        width: 304,
      }
    }
  }

  getChildContext() {
    return {
      navOpenState: this.state.navOpenState,
    };
  }

  appWithPersistentNav = () => (props) => (
    <App
      onNavResize={this.onNavResize}
      {...props}
    />
  )

  onNavResize = (navOpenState) => {
    this.setState({
      navOpenState,
    });
  }

  render() {
    return (
      <Router history={browserHistory}>
        <Route component={this.appWithPersistentNav()}>
          <Route path="/" component={Space}
          />
          <Route path="/about" component={HomePage} />
          <Route path="/space/:id" component={(match) => {
            return (
              <Space id={match.params.id} />
            )
          }} />
        </Route>
      </Router>
    );
  }
}

MainRouter.childContextTypes = {
  navOpenState: PropTypes.object,
}
